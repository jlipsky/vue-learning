export const formFieldMixin = {
  inheritAttrs: false,
  props: {
    label: String,
    value: [Number, String],
    options: Array
  },
  methods: {
    updateValue(event) {
      this.$emit('input', event.target.value)
    }
  }
}
