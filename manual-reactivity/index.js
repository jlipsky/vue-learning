let price = 5;
let quantity = 2;
let total = 0;
let storage = []; // We'll store our target functions in here

let target = () => {
  total = price * quantity;
};

let record = () => {
  // target = () => { total = price * quantity }
  storage.push(target);
};

let replay = () => {
  storage.forEach(run => run());
};

record(); // Remember this in case we want to run it later
target(); // Also go ahead and run it
