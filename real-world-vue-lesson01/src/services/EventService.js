import axios from "axios";

const apiclient = axios.create({
  baseURL: "http://localhost:3000",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

export default {
  getEvents() {
    return apiclient.get("/events");
  },
  getEvent(id) {
    return apiclient.get("/events/" + id);
  }
};
